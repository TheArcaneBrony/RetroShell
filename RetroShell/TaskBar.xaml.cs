﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RetroShell
{
    /// <summary>
    /// Interaction logic for TaskBar.xaml
    /// </summary>
    public partial class TaskBar : Window
    {
        public TaskBar()
        {
            InitializeComponent();
        }
        Screen screen;
        public void Init(Screen scr)
        {
            screen = scr;
            Width = scr.Bounds.Width;
            Left = scr.Bounds.Left;
            Top = scr.Bounds.Bottom - Height;
            Topmost = true;
        }

        private void StartButton_MouseUp(object sender, MouseButtonEventArgs e)
        {
            StartMenu sm = new StartMenu();
            sm.Show();
            sm.Left = Left;
            sm.Top = Top - sm.Height;
        }
    }
}
