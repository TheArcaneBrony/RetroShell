﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RetroShell
{
    /// <summary>
    /// Interaction logic for ShutdownMenu.xaml
    /// </summary>
    public partial class ShutdownMenu : Window
    {
        public ShutdownMenu()
        {
            InitializeComponent();
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            if(Shutdown.IsChecked.Value)
            {
                for (int i = -1; i < 1; i++)
                foreach (Screen scr in Screen.AllScreens)
                {
                    BackgroundFade fade = new BackgroundFade();
                    fade.Show();
                    fade.Init(scr,i);
                }
                foreach(string proc in new List<String>(){"chrome","spotify", "firefox", "explorer", "onedrive", "winword", "excel", "powerpnt" }) { Process.Start("taskkill", $"/im {proc}* /f"); }
                Process.Start("shutdown", $"/s /t 0 /f");
            }
        }

        private void Deny_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
