﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RetroShell
{
    /// <summary>
    /// Interaction logic for BackgroundFade.xaml
    /// </summary>
    public partial class BackgroundFade : Window
    {
        public BackgroundFade()
        {
            InitializeComponent();
        }
        Screen screen;
        public void Init(Screen scr)
        {
            screen = scr;
            Width = scr.Bounds.Width;
            Left = scr.Bounds.Left;
            Top = scr.Bounds.Top;
            Topmost = true;
            new Thread(() =>
            {
                for (int i = 0; i < screen.Bounds.Height+64; i += 16)
                {
                    Dispatcher.Invoke(() =>
                    {
                        Height = i;
                    });
                    //Thread.Sleep(50);
                }

            }).Start();
        }
        public void Init(Screen scr, int LeftOffset)
        {
            screen = scr;
            Width = scr.Bounds.Width;
            Left = scr.Bounds.Left+LeftOffset;
            Top = scr.Bounds.Top;
            Topmost = true;
            new Thread(() =>
            {
                for (int i = 0; i < screen.Bounds.Height + 64; i += 16)
                {
                    Dispatcher.Invoke(() =>
                    {
                        Height = i;
                    });
                    //Thread.Sleep(50);
                }

            }).Start();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
