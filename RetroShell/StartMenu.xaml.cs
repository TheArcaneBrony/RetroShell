﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Label = System.Windows.Controls.Label;
using MouseEventArgs = System.Windows.Input.MouseEventArgs;

namespace RetroShell
{
    /// <summary>
    /// Interaction logic for StartMenu.xaml
    /// </summary>
    public partial class StartMenu : Window
    {
        public StartMenu()
        {
            InitializeComponent();
            AddItem("a");
            AddItem("b");
            AddItem("c");
            AddItem("d");
            AddItem("e");
            
        }
        public void AddItem(string text)
        {
            Canvas cv = new Canvas();
            cv.Height = 48;
            cv.Width = 1000;
            cv.Children.Add(new Label() { Content = text });
            cv.MouseEnter += Cv_MouseEnter;
            cv.MouseLeave += Cv_MouseLeave;
            StartMenuContent_StackPanel.Children.Add(cv);
        }

        private void Cv_MouseLeave(object sender, MouseEventArgs e)
        {
            ((Canvas)sender).Background = new SolidColorBrush(Color.FromRgb(192,192,192));
        }

        private void Cv_MouseEnter(object sender, MouseEventArgs e)
        {
            ((Canvas)sender).Background = new SolidColorBrush(Color.FromRgb(0, 0, 255));
        }

        private void ShutDown_MouseUp(object sender, MouseButtonEventArgs e)
        {
            foreach (Screen scr in Screen.AllScreens)
            {
                BackgroundFade fade = new BackgroundFade();
                fade.Show();
                fade.Init(scr);
            }
            ShutdownMenu menu = new ShutdownMenu();
            menu.Show();
            Close();
        }
    }
}
